# SMTK PVServer Extension Stub Plugin

This is a pure ParaView plugin that is meant to allow the user
to save a state file in the CMB Modelbuilder application, then
switch to the ParaView application, load this plugin, and load 
the state file. SMTK specific proxies that exist in the state 
file are made available as empty stubs by this plugin, so the
state file can load without errors.

The "stub" directory was copied from [smtk paraview extension](https://gitlab.kitware.com/cmb/smtk/-/tree/master/smtk/extension/paraview/server)
and then altered:
* All references to smtk headers and classes were commented or deleted.
* Header include paths to files in this dir were changed
* module export and module header name were changed
* smconfig.xml has any classes that are still missing stubs commented.
* base CMakeLists.txt file treats this as an independent ParaView plugin.

Created Apr 2023, Aron Helser
