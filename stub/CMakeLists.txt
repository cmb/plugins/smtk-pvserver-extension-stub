set(classes
  # Registrar
  # VTKSelectionResponderGroup

  # vtkSMSMTKResourceRepresentationProxy
  # vtkSMSMTKWrapperProxy
  vtkSMTKResource
  vtkSMTKResourceCreator
  vtkSMTKResourceGenerator
  vtkSMTKResourceImporter
  vtkSMTKResourceReader
  vtkSMTKResourceRepresentation
  vtkSMTKResourceSource
  # vtkSMTKSettings
  vtkSMTKWrapper
  )

vtk_module_add_module(smtkPVServerExtStub
  CLASSES ${classes}
  )

if (ParaView_VERSION VERSION_LESS "5.10.0")
  vtk_module_definitions(smtkPVServerExtStub
    PRIVATE SMTK_PV_USE_CONFIG)
endif ()

paraview_add_server_manager_xmls(
  XMLS smconfig.xml)
