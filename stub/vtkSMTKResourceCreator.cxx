//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "../stub/vtkSMTKResourceCreator.h"

// #include "../stub/vtkSMTKWrapper.h"

// #include "smtk/attribute/ComponentItem.h"
// #include "smtk/attribute/json/jsonResource.h"

#include "vtkCompositeDataIterator.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPolyData.h"

vtkStandardNewMacro(vtkSMTKResourceCreator);

vtkSMTKResourceCreator::vtkSMTKResourceCreator()
{
  this->TypeName = nullptr;
  this->Parameters = nullptr;

  // Ensure this object's MTime > this->ModelSource's MTime so first RequestData() call
  // results in the filter being updated:
  this->Modified();
}

vtkSMTKResourceCreator::~vtkSMTKResourceCreator()
{
  this->SetTypeName(nullptr);
  this->SetParameters(nullptr);
}

void vtkSMTKResourceCreator::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "TypeName: " << this->TypeName << "\n";
  os << indent << "Parameters: " << this->Parameters << "\n";
}
