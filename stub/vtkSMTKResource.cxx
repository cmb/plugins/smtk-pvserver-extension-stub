//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "../stub/vtkSMTKResource.h"

#include "vtkMultiBlockDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPolyData.h"

#include <algorithm>

vtkStandardNewMacro(vtkSMTKResource);

vtkSMTKResource::vtkSMTKResource()
{
  this->SetNumberOfInputPorts(0);
}

vtkSMTKResource::~vtkSMTKResource()
{
}
