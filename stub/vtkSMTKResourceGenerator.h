//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtkPVServerExtStub_vtkSMTKResourceGenerator_h
#define smtkPVServerExtStub_vtkSMTKResourceGenerator_h

#include "../stub/smtkPVServerExtStubModule.h"

// #include "smtk/PublicPointerDefs.h"

#include "vtkSMTKResource.h"

/**\brief A base class for generating SMTK resources in ParaView.
 */
class SMTKPVSERVEREXTSTUB_EXPORT vtkSMTKResourceGenerator : public vtkSMTKResource
{
public:
  vtkTypeMacro(vtkSMTKResourceGenerator, vtkSMTKResource);

  vtkSMTKResourceGenerator(const vtkSMTKResourceGenerator&) = delete;
  vtkSMTKResourceGenerator& operator=(const vtkSMTKResourceGenerator&) = delete;

  // virtual smtk::resource::ResourcePtr GenerateResource() const = 0;

protected:
  vtkSMTKResourceGenerator() = default;
  ~vtkSMTKResourceGenerator() override = default;

  int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;
};

#endif
