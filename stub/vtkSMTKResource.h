//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtkPVServerExtStub_vtkSMTKResource_h
#define smtkPVServerExtStub_vtkSMTKResource_h

#include "../stub/smtkPVServerExtStubModule.h"

#include "vtkMultiBlockDataSetAlgorithm.h"
#include "vtkSmartPointer.h"
#include "vtkObjectFactory.h"

class vtkSMTKWrapper;

/**\brief A base class for manipulating SMTK resources in ParaView.
 *
 * Stub.
 */
class SMTKPVSERVEREXTSTUB_EXPORT vtkSMTKResource : public vtkMultiBlockDataSetAlgorithm
{
public:
  vtkTypeMacro(vtkSMTKResource, vtkMultiBlockDataSetAlgorithm);
  // void PrintSelf(ostream& os, vtkIndent indent) override;
  static vtkSMTKResource* New();

  vtkSMTKResource(const vtkSMTKResource&) = delete;
  vtkSMTKResource& operator=(const vtkSMTKResource&) = delete;

  /// Set the SMTK resource by the string representation of its resource Id.
  void SetResourceById(const char* resourceIdStr) {};


protected:
  vtkSMTKResource();
  ~vtkSMTKResource() override;

};

#endif
