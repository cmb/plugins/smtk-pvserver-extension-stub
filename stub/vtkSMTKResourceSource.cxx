//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "../stub/vtkSMTKResourceSource.h"

#include "vtkCompositeDataIterator.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPolyData.h"

#include <algorithm>

vtkStandardNewMacro(vtkSMTKResourceSource);

vtkSMTKResourceSource::vtkSMTKResourceSource()
{
  this->SetNumberOfInputPorts(0);
}

vtkSMTKResourceSource::~vtkSMTKResourceSource()
{
  this->SetVTKResource(nullptr);
}

void vtkSMTKResourceSource::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "VTKResource: " << this->VTKResource << "\n";
}

vtkMTimeType vtkSMTKResourceSource::GetMTime()
{
  // Access the modification time of this class ignoring the resource.
  vtkMTimeType mTime = this->vtkObject::GetMTime();

  // Access the modification time of the resource.
  vtkMTimeType resource_mTime = (this->VTKResource ? this->VTKResource->GetMTime() : mTime);

  // The outward-facing modification time of this class is the latest of these
  // two times.
  return std::max({ mTime, resource_mTime });
}

void vtkSMTKResourceSource::Modified()
{
  // Modifying this filter means marking its converter instance as modified
  // vtkAlgorithm* converter = this->VTKResource ? this->VTKResource->GetConverter() : nullptr;
  // if (converter)
  // {
  //   converter->Modified();
  // }
  this->Superclass::Modified();
}

int vtkSMTKResourceSource::FillOutputPortInformation(int port, vtkInformation* info)
{
  // We must have a resource to query for output port information.
  if (this->VTKResource == nullptr)
  {
    vtkDebugMacro("Resource is not set.");
    return 0;
  }

  if (this->VTKResource->GetNumberOfOutputPorts() > port)
  {
    vtkInformation* rinfo = this->VTKResource->GetOutputPortInformation(port);
    info->CopyEntry(rinfo, vtkDataObject::DATA_TYPE_NAME());
  }
  else
  {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMultiBlockDataSet");
  }
  return 1;
}
